# BUILD
FROM node:18-alpine3.15 AS builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

# RUN
FROM nginx:1.21.6-alpine AS run
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/dist/mastermind .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
